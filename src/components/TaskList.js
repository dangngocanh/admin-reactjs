import React, { Component } from 'react';
import TaskItem from './TaskItem';

export class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state={
      filterName :'',
      filterStatus : -1
    }
  }
  
  onChange =(event) =>{
    var target = event.target;
    var name = target.name;
    var value  = target.value;
    this.props.onFilter(
      name === 'filterName' ? value : this.state.filterName,
      name === 'filterStatus' ? value : this.state.filterStatus
    )
    this.setState({
      [name] : value
    })
  }


    render() {
        var { task } = this.props;
        var elmTask = task.map((tasks,index) => {
            return <TaskItem
                key= {tasks.id}
                index ={index}
                tasks = {tasks}
                //sửa
                onEdit ={this.props.onEdit}
                // xóa
                onDel ={this.props.onDel}              
                onUpdateStatus ={this.props.onUpdateStatus}
            />
        });
        return (
            <table className="table table-bordered table-hover mt-15">
                  <thead>
                    <tr>
                      <th className="text-center">STT</th>
                      <th className="text-center">Tên</th>
                      <th className="text-center">Trạng Thái</th>
                      <th className="text-center">Hành Động</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td />
                      <td>
                        <input 
                          type="text" 
                          name="filterName"
                          value= {this.state.filterName}
                          onChange ={ this.onChange}
                          className="form-control" />
                      </td>
                      <td>
                        <select 
                          name="filterStatus"
                          value= {this.state.filterStatus}
                          onChange ={ this.onChange}
                          className="form-control"
                        >
                          <option value={-1}>Tất Cả</option>
                          <option value={0}>Ẩn</option>
                          <option value={1}>Kích Hoạt</option>
                        </select>
                      </td>
                      <td />
                    </tr>
                    {elmTask}
                  </tbody>
                </table>
        );
    }
}

export default TaskList;
