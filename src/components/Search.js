import React, { Component } from 'react';

export class Search extends Component {
    constructor(props) {
        super(props);
        this.state ={
            keyword : ''
        }
    }
    onChange=(event) =>{
        var name = event.target.name;
        var value = event.target.value;
        this.setState({
            [name] : value
        })
    }
    onSearch =()=>{
        this.props.onSearch(this.state.keyword)
    }
    
    render() {
        return (
            <div className="input-group">
                <input 
                    type="text" 
                    name="keyword"
                    value={this.state.keyword}
                    onChange={this.onChange}
                    className="form-control" 
                    placeholder="Nhập từ khóa..." />
                <span className="input-group-btn">
                <button 
                    onClick={this.onSearch}
                    className="btn btn-primary" 
                    type="button">
                    <span className="fa fa-search mr-5" />Tìm
                </button>
                </span>
            </div>
        );
    }
}

export default Search;
