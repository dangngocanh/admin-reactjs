import React, { Component } from 'react';

export class TaskItem extends Component {
    onUpdateStatus=()=>{
        //console.log('id là : '+ this.props.tasks.id)
        // từ thằng con muốn truyền lên bố thì thông qua tham số truyền vào
        this.props.onUpdateStatus(this.props.tasks.id)
    }
    onDel=() =>{
        this.props.onDel(this.props.tasks.id)
    }
    // sẽ nhận được vào là this.props.onEdit
    onEdit =() =>{
        this.props.onEdit(this.props.tasks.id)
        //console.log(this.props.tasks.id)
        // con chứa tasks.id được thông qua tham số để truyền lên bố
    }




    render() {
        var { tasks, index } = this.props
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{tasks.name}</td>
                <td className="text-center">
                <span 
                    className={tasks.status === true ? "label label-danger" : "label label-success" }
                    onClick={this.onUpdateStatus}
                >
                   {tasks.status===true ? "Kích hoạt" : "Ẩn" }
                </span>
                </td>
                <td className="text-center">
                <button type="button" 
                    onClick ={this.onEdit}
                    className="btn btn-warning">
                    <span className="fa fa-pencil mr-5" />Sửa
                </button>
                &nbsp;
                <button type="button" className="btn btn-danger"
                    onClick={this.onDel}
                >
                    <span className="fa fa-trash mr-5" />Xóa
                </button>
                </td>
            </tr>
        );
    }
}

export default TaskItem;
