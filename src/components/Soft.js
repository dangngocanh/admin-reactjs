import React, { Component } from 'react';

export class Soft extends Component {
  


// componentWillReceiveProps(nextProps){
//   console.log(nextProps)
// }
// lưu giá trị vào state va setstate cho nó 
  onSort =(SortName, SortValue) =>{
    
    //console.log(SortName, SortValue)
    
    //console.log(this.state)
    this.props.onSort(SortName, SortValue)
  }


    render() {
        return (
            <div className="dropdown">
                  <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                    Sắp Xếp <span className="fa fa-caret-square-o-down ml-5" />
                  </button>
                  <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li onClick={()=>this.onSort("name",1)}>
                      <div role="button">
                        <span className="fa fa-sort-alpha-asc pr-5">
                          Tên A-Z
                        </span>
                      </div>
                    </li>
                    <li onClick={()=>this.onSort("name",-1)}>
                      <div role="button">
                        <span className="fa fa-sort-alpha-desc pr-5">
                          Tên Z-A
                        </span>
                      </div>
                    </li>
                    <li role="separator" className="divider" />
                    <li onClick={()=>this.onSort("status",1)}>
                    <div role="button">Trạng Thái Kích Hoạt</div></li>
                    <li onClick={()=>this.onSort("status",-1)}>
                    <div role="button">Trạng Thái Ẩn</div></li>
                  </ul>
                </div>
        );
    }
}

export default Soft;
