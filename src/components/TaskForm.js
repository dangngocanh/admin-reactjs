import React, { Component } from 'react';

class TaskForm extends Component {

        constructor(props) {
            super(props);
            this.state={
                id: "",
                name: "", // name này phải cùng tên chứa name của input,
                status: false, // mặc định khi hiện form trạng thái sẽ auto là ẩn vì false là ẩn
            }
        }
        UNSAFE_componentWillMount(){
            if(this.props.taskEditing){
                this.setState({
                    id: this.props.taskEditing.id,
                    name: this.props.taskEditing.name,
                    status: this.props.taskEditing.status
                })
            }
        }
        /// khi bấm nút sửa vẫn nhận đc props
        UNSAFE_componentWillReceiveProps(nextProps){
            // nếu nextProps tồn tại và nextProps.taskEditing tồn tại
            // nextProps !== null
            if(nextProps && nextProps.taskEditing){
                this.setState({
                    id: nextProps.taskEditing.id,
                    name: nextProps.taskEditing.name,
                    status: nextProps.taskEditing.status
                })
            }else if (nextProps.taskEditing === null){
                console.log("sửa > thêm")
                this.setState({
                    id: '',
                    name: '',
                    status: this.state.status
                })
            }
        }


          // ở đây sẽ nhận vào 1 props là this.props.onCloseForm
          onCloseForm =()=>{
            this.props.onCloseForm()
        }

        onChangeForm =(event)=>{
            var target = event.target;
            var name = target.name;
            var value = target.value;
            if(name === 'status'){
              value = target.value ==='true'? true: false;
            }
            this.setState({
                [name] : value
            })
        }
        onSubmit = (event) => {
            event.preventDefault(); // loại bỏ khi submit load lại trang mặc đinh của trang
           this.props.onSubmit(this.state);// thằng con nhận đc props của bố và gán tham số state sang bố
           this.onClear();
           this.onCloseForm()
          console.log(this.state)
        }
    
        onClear =()=> {
            this.setState({
                name : "",
                status : false
            });
        }
        
  render() {
      var { id } = this.state
    return (
        <div className="panel panel-warning">
            <div className="panel-heading">
                <h3 className="panel-title">{id !== "" ? "Sửa Công Việc" : "Thêm Công Việc" }   <span className="text-right fa fa-times-circle " 
                onClick={this.onCloseForm}
                ></span></h3>
               

            </div>
            
            <div className="panel-body">
                <form id="client" onSubmit={this.onSubmit}>
                    <div className="form-group">
                    <label>Tên :</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        // làm việc với form t phải có name và value và onChange
                        name = "name" // trùng với name của state
                        value ={this.state.name}
                        onChange={this.onChangeForm}
                    />
                    </div>
                    <label>Trạng Thái :</label>
                    
                    <select 
                        name = "status" // trùng với name của state
                        value ={this.state.status}
                        onChange={this.onChangeForm}
                        className="form-control" 
                        required="required">
                    <option value={true}>Kích Hoạt</option>
                    <option value={false}>Ẩn</option>
                    </select>
                    <br />
                    <div className="text-center">
                    <button
                        type="submit" 
                        className="btn btn-warning"
                    >{id !== "" ? "Sửa" : "Thêm" }</button>&nbsp;  
                    <button
                        type="button" 
                       onClick={this.onClear}
                        className="btn btn-danger"
                    >Hủy Bỏ</button>
                    </div>
                </form>
                
            </div>
        </div>

    );
  }
}

export default TaskForm;