//console.log("Học Redux")


// đầu tiên phải import  creatStore
import { createStore } from 'redux';

// store cần có reducer
// reducer function nhận vào các action trả ra state mới 
var startState = {
    status : false,
    sort: {
        by: "name",
        value : 1// 1 là tăng -1 là giảm do tự đặt để làm mục sắp sếp
    }
}
// có 1 reducer là function
// nhận vào 2 tham số là state, action
// và giá trị mặc định state = startStatus
// mục đích của reducer là trả ra state mới
var myReducer = (state = startState, action) => {
    return state;
}
//------------------------------------------------

const store = createStore(myReducer);
// tạo action kh người dùng click vào
// thay đổi status
var action = {
    type: 'TOGGLE_STATUS'
};
/// store sẽ gọi dispatch() để truyền vào action
store.dispatch(action)






console.log(store)
// log ra hiện ra dispatch(), getState(), replaceReduce(), subcribe()
console.log(store.getState()) // lấy ra được state