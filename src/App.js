import React, { Component } from 'react';
import TaskForm from './components/TaskForm';
import Control from './components/Control';
import TaskList from './components/TaskList';
import './App.css';
import demo from './trainning/demo'; // import file demo.js




//import _ from 'lodash';
class App extends Component {

  constructor(props) {
    super(props);
    this.state={
      // tạo một task mảng rỗng
      task: [],
      isShowForm: false,
      taskEditing : null,
      filter : {
        name : '',
        status : -1
      },
        sortBy : "",
        sortValue: 1
    }
  }
  //để lưu giá trị localstorage cần sử dung lifecycle
  //componentWillMount nó chỉ được gọi duy nhất 1 lần
  UNSAFE_componentWillMount() {
    // kiểm tra khang null
    if(localStorage && localStorage.getItem('task')){
      var task = JSON.parse(localStorage.getItem('task'));
      this.setState({
        task: task
      });
    }
  }
  // tạo id random-------------------------------------
  iD(){
    return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
  }
  ranDom(){
    return this.iD() + this.iD() + '-' + this.iD()
  }
//-------------------------------------- 
// tạo toggle form khi ng dùng click vào thêm công việc
onToggleFrom=()=>{
  if(this.state.isShowForm && this.state.taskEditing !==null){
    this.setState({
      isShowForm : true,
      taskEditing : null
    })
  }else{
    this.setState({
      isShowForm : !this.state.isShowForm,
      taskEditing : null
    })
  }
  
}

  onCloseForm=()=>{
    this.setState({
      isShowForm : false
    })
  }

  onShowForm=()=>{
    this.setState({
      isShowForm : true
    })
  }

  
  onSubmit = (data) =>{
    //// để thực hiện khi click vào sửa và sửa xong rồi bấm sửa thì thông qua đc thằng id để nhận biết và sửa đc
     var { task } = this.state
     // check console.log(data) ra ta thấy đc id ="" khi thêm vào thì nó sẽ có id bằng rỗng.
     if(data.id === ""){
        data.id = this.iD(); // lấy ra id đ rundom
        task.push(data) // push vào 1 mảng
     }else {
       var index = this.findIndex(data.id)
       console.log(index)
       task[index] = data
     }
     
      this.setState({
        task : task,
        taskEditing : null
   })
  console.log(this.state)
  localStorage.setItem('task', JSON.stringify(task))

  }
  // lúc này onUpdateStatus được truyền vào this.props.tasks.id 
  onUpdateStatus= (id) =>{
   // console.log(id)
    var { task } = this.state ;
    var index = this.findIndex(id)
    //console.log(index)
      if(index!== -1){
        task[index].status = !task[index].status
        this.setState({
          task : task
        })
        localStorage.setItem('task',JSON.stringify(task))
      }
  }
  findIndex=(id)=>{
    // id này là tham số truyền vào của thằng con lên cha
    var { task } = this.state ;
    var result= -1;
    task.forEach((e, index) => {
      if(e.id === id){
        result = index 
       
      }
    });
    return result
    // hàm này đc trả về có tham số là result
    //console.log('đây là của findIndex '+ result)
  }


  onDel=(id)=>{
    var { task } = this.state ;
    var index = this.findIndex(id)

    //console.log(index)
        if(index !== -1){
          // index !==-1 nếu tồn tại mới thực hiện
          task.splice(index, 1);
          this.setState({
            task : task
          })
        localStorage.setItem('task',JSON.stringify(task))
      }
      this.onCloseForm()
  }
    onEdit=(id) =>{
      //console.log(id)
      var { task } = this.state ;
      var index = this.findIndex(id);
      var taskEditing = task[index];
      //console.log(taskEditing)
      this.setState({
        taskEditing : taskEditing
      })
      //console.log(this.state.taskEditing)
      this.onShowForm()
    }
    // lọc bảng table
    onFilter = (filterName, filterStatus) => {
     // console.log(filterName +'và'+ filterStatus)
      filterStatus = parseInt(filterStatus);
     // console.log(typeof(filterStatus))
      this.setState({
        filter : {
          name : filterName.toLowerCase(),
          status: filterStatus
        }
      })
    }

    onSearch = (keyword) =>  {
      // keyword : là tham số con truyền bố
      //console.log(keyword)
      this.setState({
        keyword : keyword.toLowerCase()
      })
    }

    ///----------------------------------------
    onSort =(sortBy,sortValue) => {
      //console.log(sortBy + sortValue)
      this.setState({
        sortBy : sortBy,
        sortValue : sortValue
      })
      //console.log(this.state)
    }




  render() {
    var { task, isShowForm, taskEditing, filter, keyword, sortBy, sortValue} = this.state // this.state.task
   // console.log(filter)
    if(filter){
      if(filter.name) {// để trường hợp  filter.name sẽ kiểm tra tất cả !==null ,!==undefined, !== 0
        task = task.filter((e)=> {
          return e.name.toLowerCase().indexOf(filter.name) !== -1
        })
      } 
      task = task.filter((e)=> {
        if(filter.status === -1 ){
          return e;
        }else{
          return e.status === (filter.status === 1 ? true : false)
        }
      });
    
      
    }
    if (keyword){
        task = task.filter((e)=> {
          return e.name.toLowerCase().indexOf(keyword) !== -1
        })
    }
    if(sortBy==="name") {
      // sap sep tang dan
      task.sort((a,z)=> {
       if(a.name > z.name){
        return -sortValue
       }else if(a.name<z.name){
        return sortValue
       }else{
         return 0
       }
      })
    }else{
      task.sort((a,z)=> {
        if(a.status > z.status){
         return -sortValue
        }else if(a.status<z.status){
         return sortValue
        }else{
          return 0
        }
       })
    }
    //--------------------------------
    var elmShowForm = isShowForm === true ? <TaskForm 
          taskEditing = {taskEditing}
          onSubmit = {this.onSubmit}
          onCloseForm ={this.onCloseForm}/> : '' ;
    return (
      <div className="container">
        <div className="text-center">
          <h1>Quản Lý Công Việc</h1>
          <hr />
        </div>
        <div className="row">
          <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {/* TaskForm */}
            {elmShowForm}
          </div>
          <div className={isShowForm ? "col-xs-8 col-sm-8 col-md-8 col-lg-8" : "col-xs-12 col-sm-12 col-md-12 col-lg-12"}>
            <button
             type="button" 
             className="btn btn-primary" 
             onClick={ this.onToggleFrom }
             >
              <span className="fa fa-plus mr-5" />Thêm Công Việc
            </button>
            {/* <button type="button" className="btn btn-primary" onClick={this.onListData}>
              <span className="fa fa-plus mr-5" />List Data
            </button> */}

      {/* Control */}
              <Control 
                  onSearch={this.onSearch}
                  onSort ={this.onSort}
                  sortBy = {this.state.sortBy}
                  sortValue = { this.state.sortValue}
                  />
      
            <div className="row mt-15">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <TaskList
                  onFilter = {this.onFilter}
                  onEdit = {this.onEdit}
                  onUpdateStatus ={this.onUpdateStatus}
                  onDel = {this.onDel}
                task ={task}/>

              </div>
            </div>
          </div>
        </div>
    </div>

    );
  }
}

export default App;